// チャートの種類を定義
export const BAR_CHART = 0;
export const LINE_CHART = 1;
export const RADAR_CHART = 2;
export const PIE_CHART = 3;

// メニュ表示番号定義
export const DASHBOARD = 0;
export const CUSTOMERS = 1;

export const barChartData = {
    labels: ['January', 'February', 'March'],
    datasets: [{
        label: ['BarChart Exsample'],
        backgroundColor: ['#EE6E85', '#F19642', '#F8CE6B'],
        data: [90, 80, 75]
    }]
};

export const lineChartData = {
    labels: ['January', 'February', 'March'],
    datasets: [{
        label: 'Apple',
        data: [12, 19, 3],
        backgroundColor: '#F6B6C2',
        borderColor: '#F6B6C2',
        fill: false
    }, {
        label: 'Orange',
        data: [2, 29, 5],
        backgroundColor: 'rgba(255,153,0,0.4)',
        borderColor: 'rgba(255,153,0,0.4)',
        fill: false
    }]
};

export const radarChartData = {
    labels: ['January', 'February', 'March', 'April', 'May'],
    datasets: [{
        label: 'Exsample1',
        backgroundColor: "#EE6E85",
        borderColor: "#EE6E85",
        fill: false,
        data: [10, 11, 12, 13, 14]
    }, {
        label: 'Exsample2',
        backgroundColor: "#F19642",
        borderColor: "#F19642",
        fill: false,
        data: [14, 13, 12, 11, 10]
    }]
};

export const pieChartData = {
    labels: ['January', 'February', 'March'],
    datasets: [{
        backgroundColor: ['#EE6E85', '#F19642', '#F8CE6B'],
        data: [12, 19, 3]
    }]
}

export const chartOption = {
    legend: {
        display: false
    },
    title: {
        display: true,
        text: 'Chart Exsamples'
    }
};

/**
 * チャートを描画する.
 * 
 * @param {HTMLElement} elCtx 描画する要素指定
 * @param {string} chartType チャートの種類を指定
 * @param {Object} chartData チャートのデータを指定
 * @param {Object} chartOption チャートのオプションを指定
 * @returns 
 */
export function drawChart(elCtx, chartType, chartData, chartOption) {
    return new Chart(elCtx, {
        type: chartType,
        data: chartData,
        option: chartOption,
    });
}