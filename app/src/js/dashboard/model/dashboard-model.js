export const DashboardMenuObsItem = h5.core.data.createObservableItem({
    name: 'DashboardMenuObsItem',
    // 選択されているメニュー(0〜)
    currentMenu: {
        type: 'integer',
        defaultValue: 0
    },
});