import {
    barChartData,
    lineChartData,
    radarChartData,
    pieChartData,
    chartOption,
    drawChart,
    BAR_CHART,
    LINE_CHART,
    RADAR_CHART,
    PIE_CHART,
} from '../model/chart-model.js';

import {
    CustomersLogic
} from '../logic/customers-logic.js';

export const CustomersController = {

    // 本コントローラ名称
    __name: 'CustomersController',

    // 依存するロジック
    _customerLogic: CustomersLogic,

    __ready: function (context) {
        // 前の画面からパラメータを受け取り描画する
        this.toggleChart(context.args.chartType);

        this._customerLogic.getPosts().then((posts) => {
            h5.core.view.bind('#posts', {
                items: _.take(posts, 5)
            });
        });
    },
            
    /**
     * URLに指定されたパラメータより、画面に描画する図を決定し画面に反映する。
     * 
     * @param {number} selectedNum 
     */
    toggleChart: function (selectedNum) {
        const ctxBarChart = this.$find('#chart')[0].getContext('2d');
        let title;
        switch (selectedNum) {
            case BAR_CHART:
            default:
                drawChart(ctxBarChart, 'bar', barChartData, chartOption);
                title = '棒グラフ';
                break;

            case LINE_CHART:
                drawChart(ctxBarChart, 'line', lineChartData, chartOption);
                title = '折れ線グラフ';
                break;

            case RADAR_CHART:
                drawChart(ctxBarChart, 'radar', radarChartData, chartOption);
                title = 'レーダーチャート';
                break;

            case PIE_CHART:
                drawChart(ctxBarChart, 'pie', pieChartData, chartOption);
                title = '円グラフ';
                break;
        }
        // タイトルを設定
        this.$find('p.title').text(title);
    }
};