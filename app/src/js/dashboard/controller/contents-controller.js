import {
    DashboardMenuObsItem
} from '../model/dashboard-model.js';

import {
    DASHBOARD,
    CUSTOMERS
} from '../model/chart-model.js';

import {
    DashboardController
} from './dashboard-controller.js';

import {
    CustomersController
} from './customers-controller.js';


export const ContentsController = {

    // 本コントローラ名称
    __name: 'ContentsController',

    // メニューの状態を管理
    _dashboardMenuObsItem: DashboardMenuObsItem,

    __ready: function () {
        h5.core.expose(DashboardController);
        h5.core.expose(CustomersController);

        // デフォルトで1ページ目を表示
        h5.scene.getMainSceneContainer().navigate({
            navigationType: 'silent',
            to: '/src/html-partials/example1.html',
        });

        this._dashboardMenuObsItem.addEventListener('change', () => {
            this.toggleMenu();
            this.transition();
        });
    },

    /**
     * バーガーメニューの制御
     * 
     * @param {any} context イベントコンテキスト
     * @param {any} $el イベント発生元
     */
    '#burger click': function (context, $el) {
        $el.toggleClass('is-active');
        this.$find('#navbar-menu').toggleClass('is-active');
    },

    /**
     * 上部ナビゲーションバーの制御
     * 
     * @param {any} context イベントコンテキスト
     * @param {any} $el イベント発生元
     */
    '#navbar-menu .navbar-item click': function (context, $el) {
        this.$find('#burger').trigger('click');
        const navbarMenu = this.$find('#navbar-menu .navbar-item').removeClass('is-active');
        const selectedItemNum = $.inArray($el.context, navbarMenu);
        this._dashboardMenuObsItem.set('currentMenu', selectedItemNum);
    },

    /**
     * サイドナビゲーションバーの制御
     * 
     * @param {any} context 
     * @param {any} $el 
     */
    '#menu a click': function (context, $el) {
        const elMenuItems = this.$find('#menu a');
        const selectedItemNum = $.inArray($el.context, elMenuItems);
        this._dashboardMenuObsItem.set('currentMenu', selectedItemNum);
    },

    /**
     * ナビゲーションバーを装飾する
     * 
     */
    toggleMenu: function (num) {
        let selectedNum;
        if (arguments.length === 1) {
            selectedNum = num;
        } else {
            selectedNum = this._dashboardMenuObsItem.get('currentMenu');
        }

        // 上部ナビゲーションバーの装飾
        const navbarMenu = this.$find('#navbar-menu .navbar-item');
        navbarMenu.removeClass('is-active');
        $(navbarMenu[selectedNum]).addClass('is-active');

        // サイドナビゲーションバーの装飾
        const sidenavMenu = this.$find('#menu a');
        sidenavMenu.removeClass('is-active');
        $(sidenavMenu[selectedNum]).addClass('is-active');
    },

    /**
     * 画面遷移を行う
     * 
     * @param {any} selectedNum 選択された要素番号(0〜)
     */
    transition: function (arg) {
        let selectedNum
        let chartType = 0;
        if (arguments.length === 1) {
            selectedNum = arg.numPage;
            chartType = arg.numChatType;
        } else {
            selectedNum = this._dashboardMenuObsItem.get('currentMenu');
        }

        // 画面遷移
        switch (selectedNum) {
            case DASHBOARD:
            default:
                h5.scene.getMainSceneContainer().navigate({
                    to: '/src/html-partials/example1.html',
                });
                break;

            case CUSTOMERS:
                h5.scene.getMainSceneContainer().navigate({
                    to: '/src/html-partials/example2.html',
                    args: {
                        chartType: chartType,
                    },
                });
                break;
        }
    }
};