import {
    barChartData,
    lineChartData,
    radarChartData,
    pieChartData,
    chartOption,
    drawChart,
} from '../model/chart-model.js';

export const DashboardController = {

    // 本コントローラ名称
    __name: 'DashboardController',

    __ready: function () {
        // 棒グラフの描画
        const ctxBarChart = this.$find('#bar-chart')[0].getContext('2d');
        drawChart(ctxBarChart, 'bar', barChartData, chartOption);

        // 折れ線グラフの描画
        const ctxLineChart = this.$find('#line-chart')[0].getContext('2d');
        drawChart(ctxLineChart, 'line', lineChartData, chartOption);

        // レーダーチャートの描画
        const ctxRadarChart = this.$find('#radar-chart')[0].getContext('2d');
        drawChart(ctxRadarChart, 'radar', radarChartData, chartOption);

        //円グラフの描画
        const ctxPieChart = this.$find('#pie-chart')[0].getContext('2d');
        drawChart(ctxPieChart, 'pie', pieChartData, chartOption);
    },

    /**
     * チャート詳細ボタンの制御
     * 
     * @param {any} context 
     * @param {any} $el 
     */
    '.button click': function (context, $el) {
        const elButtons = this.$find('.button');
        const selectedChartType = $.inArray($el.context, elButtons);

        h5.scene.getMainSceneContainer().navigate({
            to: '/src/html-partials/example2.html',
            args: {
                chartType: selectedChartType,
            },
        });
    },
};