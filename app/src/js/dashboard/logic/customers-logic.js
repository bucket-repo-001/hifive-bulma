const URL_TEST_SITE = 'https://jsonplaceholder.typicode.com/';

export const CustomersLogic = {

    // 本ロジック名称
    __name: 'CustomersLogic',

    __ready() {

    },

    async getPosts() {
        return this.load(URL_TEST_SITE + 'posts');
    },

    /**
     * ajax通信でサーバのリソースを取得
     * 
     * @export
     * @param {string} url 
     * @returns 
     */
    async load(url) {
        return fetch(url)
            .then((response) => response.json())
    },
}