const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin('[name].css');

module.exports = [
    {
        entry: {
            main: './scss/main.scss'
        },
        output: {
            path: path.resolve(__dirname, 'app/src/css'),
            publicPath: '/css/',
            filename: '[name].css'
        },
        module: {
            rules: [{
                    test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [{
                        loader: 'url-loader'
                    }]
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: extractSass.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'sass-loader']
                    })
                }
            ]
        },
        devServer: {
            contentBase: path.resolve(__dirname, 'app'),
            publicPath: './app',
            watchContentBase: true
        },
        plugins: [
            extractSass
        ],
        resolve: {
            extensions: ['.css', '.js']
        },
    },
];